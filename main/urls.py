from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('find-book/', views.findBook, name ='findBook'),
    path('process-data/', views.processJSONData, name = 'processJSONData'),
]
