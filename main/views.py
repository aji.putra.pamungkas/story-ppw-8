from django.shortcuts import render
from django.http import JsonResponse
import requests, json

def home(request):
    return render(request, 'main/home.html')

def findBook(request):
    response = {}
    return render(request, 'main/findBook.html', response)

def processJSONData(request):
    arg = request.GET.get('q')
    str_arg = str(arg)
    url_tujuan = "https://www.googleapis.com/books/v1/volumes?q=" + str_arg
    data_JSON = requests.get(url_tujuan)
    data_fix = json.loads(data_JSON.content)

    return JsonResponse(data_fix, safe ="false")
