from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home, findBook, processJSONData
import requests

"""
@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    Base class for functional test cases with selenium.

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
"""

class mainTest(TestCase):
    def test_main_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_find_book_url_is_exist(self):
        response = Client().get('/find-book/')
        self.assertEqual(response.status_code,200)

    def test_main_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')
    
    def test_find_book_using_findBook_template(self):
        response = Client().get('/find-book/')
        self.assertTemplateUsed(response, 'main/findBook.html')

    def test_main_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_find_book_using_func(self):
        found = resolve('/find-book/')
        self.assertEqual(found.func, findBook)

    def test_process_data_url_is_exist(self):
        response = Client().get('/process-data/')
        self.assertEqual(response.status_code, 200)

    def test_process_data_using_func(self):
        found = resolve('/process-data/')
        self.assertEqual(found.func, processJSONData)
